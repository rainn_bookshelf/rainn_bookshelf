from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from .models import Post, Comment, Category
from .forms import PostForm, CommentForm
from django.views import generic

def detail_post(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    context = {'post': post}
    return render(request, 'posts/detail.html', context)

def list_posts(request):
    post_list = Post.objects.all()
    context = {'post_list': post_list}
    return render(request, 'posts/index.html', context)

def search_posts(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        post_list = Post.objects.filter(name__icontains=search_term)
        context = {"post_list": post_list}
    return render(request, 'posts/search.html', context)

def create_post(request):
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            post_name = form.cleaned_data['name']
            post_date = form.cleaned_data['date']
            post_rating = form.cleaned_data['rating']
            post_image_url = form.cleaned_data['image_url']
            post_review = form.cleaned_data['review']
            post = Post(name=post_name,
                        date=post_date,
                        rating = post_rating,
                        image_url=post_image_url,
                        review = post_review)
            post.save()
            return HttpResponseRedirect(
                reverse('posts:detail', args=(post.id, )))

    else:
        form = PostForm()
    context = {'form': form}
    return render(request, 'posts/create.html', context)


def update_post(request, post_id):
    post = get_object_or_404(Post, pk=post_id)

    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post.name = form.cleaned_data['name']
            post.date = form.cleaned_data['date']
            post.rating = form.cleaned_data['rating']
            post.image_url = form.cleaned_data['image_url']
            post.review = form.cleaned_data['review']
            post.save()
            return HttpResponseRedirect(
                reverse('posts:detail', args=(post.id, )))

    else:
        form = PostForm(
            initial={
                'name': post.name,
                'date': post.date,
                'rating': post.rating, 
                'image_url': post.image_url,
                'review': post.review
            })

    context = {'post': post,'form':form}
    return render(request, 'posts/update.html', context)


def delete_post(request, post_id):
    post = get_object_or_404(Post, pk=post_id)

    if request.method == "POST":
        post.delete()
        return HttpResponseRedirect(reverse('posts:index'))

    context = {'post': post}
    return render(request, 'posts/delete.html', context)


def create_comment(request, post_id):
    post = get_object_or_404(Post, pk=post_id)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment_author = form.cleaned_data['author']
            comment_text = form.cleaned_data['text']
            comment_date = form.cleaned_data['date']
            comment = Comment(author=comment_author,
                            text=comment_text,
                            date = comment_date,
                            post=post)
            comment.save()
            return HttpResponseRedirect(
                reverse('posts:detail', args=(post_id, )))
    else:
        form = CommentForm()
    context = {'form': form, 'post': post}
    return render(request, 'posts/comment.html', context)


class CategoryListView(generic.ListView):
    model = Category
    template_name = 'posts/categories.html'

def detail_category(request, category_id):
    category = get_object_or_404(Category, pk=category_id)
    context = {'category': category}
    return render(request, 'posts/category.html', context)