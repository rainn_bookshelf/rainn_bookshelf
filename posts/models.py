from django.db import models
from django.conf import settings


class Post(models.Model):
    name = models.CharField(max_length=255)
    date = models.DateTimeField()
    rating = models.CharField(max_length=255)
    image_url = models.URLField(max_length=200, null=True)
    review = models.TextField()

    def __str__(self):
        return f'{self.name} ({self.date})'

class Comment(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    date = models.DateTimeField()
    post = models.ForeignKey(Post, on_delete=models.CASCADE)

    def __str__(self):
        return f'"{self.text}" - {self.author.username}'

class Category(models.Model):
    name = models.CharField(max_length=255)
    posts = models.ManyToManyField(Post)

    def __str__(self):
        return f'{self.name}'