from django.forms import ModelForm
from .models import Post, Comment


class PostForm(ModelForm):
    class Meta:
        model = Post
        fields = [
            'name',
            'date',
            'rating',
            'image_url',
            'review',
        ]
        labels = {
            'name': 'Name',
            'date' : 'Date',
            'rating' : 'Rating',
            'image_url' : 'Image URL',
            'review' : 'Review',
        }

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        fields = [
            'author',
            'text',
            'date',
        ]
        labels = {
            'author': 'User',
            'text': 'Comment',
            'date': 'Date'
        }